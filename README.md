# Bubble game

[Link to webapp](https://ikinami.gitlab.io/bubbles)

This is a small game around revealing an image by hovering your mouse over circles.
I have created a small Javascript implementation to be run in the browser using the HTML5 web API.

For best results use an image that is square aspect ratio, and upload to a location that sets the correct CORS headers.

Enjoy!

Made with ❤️ by [Kinami Imai 今井きなみ](https://ikinami.gitlab.io/kinami/)
