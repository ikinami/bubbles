const animationDuration = 250;
const d = 128;
const scale = 2;

let img;

const initImg = () => {
    img = [];
    for (let k = d, depth = 0; k > 1; k /= 2){
	    img[depth] = [];
	    for(let i = 0; i < k; i++){
		    img[depth][i] = [];
		    for(let j = 0; j < k; j++)
			    img[depth][i][j] = {r:0, g:0, b:0};
	    }
	    depth++;
    }
}


const displayError = (e) => {
    const error = document.getElementById('error');
    error.style.display = 'block';
    error.innerText = `There was an error loading the image, please ensure you are using an image which is available and has the correct headers set. If you are not sure what that means, try saving the image and uploading it to a different service. Technical details: ${e}`
}

const h_mean = (values) => Math.sqrt(
        values.reduce((a,v) => a+v**2, 0)/values.length
);

const blur = (pixel) => ['r', 'g', 'b'].reduce((a,k) => ({
    ...a,
    [k]: h_mean(pixel.map(e => e[k]))
}), {});

const animation = (i, j, k) => {
	document.getElementById(i+"/"+j+"/"+k).className = "fadeOut";
}

const drawCircle = (size, r, g, b, j, k) => {
	rad = scale;
	for(i = 0; i < size; i++)
		rad *= 2;
	rad = parseInt(rad);
	return `<svg width="${rad*2}" height="${rad*2}" style="">
	    <circle
	        id="circle${size}/${j}/${k}"
	        cx="${rad}"
	        cy="${rad}"
	        r="${rad}"
	        fill="rgb(${parseInt(r)},${parseInt(g)},${parseInt(b)})"
	        onmouseover="wrapper(${i},${j},${k})"
	    />
	</svg>`;
};

const wrapper = (i,j,k) => {
	document.getElementById("circle"+i+"/"+j+"/"+k).onmouseover = "";   //Protect against double calls
	setTimeout(() => {
	    animation(i,j,k);
	    setTimeout(function(){spawnCircle(i,j,k)}, animationDuration);
	},animationDuration);
}

const spawnCircle = (i,j,k) => {
	if(i == 0) return;
	let current = document.getElementById(i+"/"+j+"/"+k);
	let size = scale;
	for (let a = 0; a < i; a++)
		size *= 2;
    const makeNewDiv = (j,k) => `<div
        class='fadeIn'
        id='${(i-1)}/${j}/${k}'
        style='float:left;
        width:${parseInt(size)}px;
        height:${parseInt(size)}px;
        font-size: 1px'
    >
        ${drawCircle(
            i-1,
            img[i-1][j][k].r,
            img[i-1][j][k].g,
            img[i-1][j][k].b,
            j,
            k
        )}
    </div>`
	current.innerHTML =
	    makeNewDiv(j*2, k*2)            // Top Left
        + makeNewDiv(j*2, (k*2+1))      // Top Right
        + makeNewDiv((j*2+1), k*2)      // Bottom Left
        + makeNewDiv((j*2+1), (k*2+1))  // Bottom Right
}

const changeImage = () => {
    const newImage = document.getElementById('image_input').value;
    document.getElementById("image_source").src = newImage;
    document.getElementById('link').href = `${window.location}?i=${newImage}`;
    document.getElementById('link').innerText = `${window.location}?i=${newImage}`;
}

const make_circles = () => {
    try {
        initImg();
        const c = document.getElementById("input_canvas");
        const ctx = c.getContext("2d");
        const imgz = document.getElementById("image_source");
            
        const urlParams = Object.fromEntries(
            [...new URLSearchParams(window.location.search)]
        );
        
        if (urlParams.i) {
            document.getElementById("editor").style.display = 'none';
        }

        if (imgz.src != urlParams.i && urlParams.i) {
            imgz.src = urlParams.i;
            return;
        }
	    c.width = imgz.width;

	    c.height = imgz.height;
        ctx.drawImage(imgz, 0, 0);
        let imgData = ctx.getImageData(0, 0, c.width, c.height);
        let i;
        let j;
        let k;
        let offset;
        if(c.width > c.height)
            offset = c.width/d;
        else
            offset = c.height/d;
        let img2 = ctx.getImageData(0,0,d,d);
        for(i = 0; i < d; i++)
            for (j = 0; j < d; j++) {
                if (offset*j < c.width && offset*i < c.height) {
                    img2.data[(i*d*4)+j*4] = imgData.data[(parseInt(i*offset)*c.width*4)+4*parseInt(j*offset)];
                    img[0][i][j].r = img2.data[(i*d*4)+j*4];
                    img2.data[(i*d*4)+j*4+1] = imgData.data[(parseInt(i*offset)*c.width*4)+4*parseInt(j*offset)+1];

                    img[0][i][j].g = img2.data[(i*d*4)+j*4+1];
                    img2.data[(i*d*4)+j*4+2] = imgData.data[(parseInt(i*offset)*c.width*4)+4*parseInt(j*offset)+2];
                    img[0][i][j].b = img2.data[(i*d*4)+j*4+2];
                    img2.data[(i*d*4)+j*4+3] = 255;

                }else{
                    img2.data[(i*d*4)+j*4] = imgData.data[imgData.data.length-4];
                    img2.data[(i*d*4)+j*4+1] = imgData.data[imgData.data.length-3];
                    img2.data[(i*d*4)+j*4+2] = imgData.data[imgData.data.length-2];
                    img2.data[(i*d*4)+j*4+3] = 0;

                    img[0][i][j].r = img2.data[(i*d*4)+j*4];
                    img[0][i][j].g = img2.data[(i*d*4)+j*4+1];
                    img[0][i][j].b = img2.data[(i*d*4)+j*4+2];
                }

            }

        for (i = 1; i < img.length; i++)
	        for (j = 0; j < img[i].length; j++)
		        for (k = 0; k < img[i][j].length; k++) {
			        img[i][j][k] = blur([
			            img[i-1][j*2][k*2],
			            img[i-1][j*2+1][k*2],
			            img[i-1][j*2+1][k*2+1],
			            img[i-1][j*2][k*2+1]
			        ]);

			        img2.data[((j*d)+k)*4] = img[i][j][k].r;
			        img2.data[((j*d)+k)*4+1] = img[i][j][k].g;
			        img2.data[((j*d)+k)*4+2] = img[i][j][k].b;
		        }
        for(i = 0; i < img2.data.length; i+=4){
	        img2.data[i] = img[img.length-1][0][0].r;
	        img2.data[i+1] = img[img.length-1][0][0].g;
	        img2.data[i+2] = img[img.length-1][0][0].b;
        }

        for (i = 0; i < imgData.data.length; i += 4) {
            imgData.data[i] = 255 - imgData.data[i];
            imgData.data[i+1] = 255 - imgData.data[i+1];
            imgData.data[i+2] = 255 - imgData.data[i+2];
            imgData.data[i+3] = 255;
        }
        ctx.putImageData(img2, 0, 0);
	    let cat = document.getElementById("7/0/0");
	    cat.id = img.length+"/0/0";

	    cat.innerHTML = drawCircle(
	        img.length, parseInt(img[img.length-1][0][0].r), 
	        parseInt(img[img.length-1][0][0].g),
	        parseInt(img[img.length-1][0][0].b), 0, 0
	    );
	    cat.style.height = d*scale*2+"px";
	    cat.style.width = d*scale*2+"px";
    } catch (e) {
         displayError(e.message);
    }
};
